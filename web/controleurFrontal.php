<?php

////////////////////
// Initialisation //
////////////////////

use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../vendor/autoload.php';

/////////////
// Routage //
/////////////
///
///
$requete = Request::createFromGlobals();

try {
    TheFeed\Controleur\RouteurURL::traiterRequete($requete)->send();
} catch (Exception $e) {
}

