<?php

namespace TheFeed\Controleur;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurServiceInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurUtilisateur extends ControleurGenerique
{
    public function __construct(ContainerInterface $container, private UtilisateurServiceInterface $utilisateurService, private PublicationServiceInterface $publicationService){
        parent::__construct($container);
    }

    public function afficherErreur($messageErreur = "", $statusCode = 400): Response
    {
        return parent::afficherErreur($statusCode, "utilisateur");
    }


    /**
     * @throws ServiceException
     * @throws Exception
     */
    #[Route(path: '/utilisateurs{idUtilisateur}/publications', name: 'afficherPublications', methods: ['GET'])]
    public function afficherPublications($idUtilisateur): Response
    {
        $utilisateur = $this->utilisateurService->recupererUtilisateurParId($idUtilisateur, false);
        if ($utilisateur === null) {
            MessageFlash::ajouter("error", "Login inconnu.");
            return $this->rediriger("afficherListe");
        } else {
            $loginHTML = htmlspecialchars($utilisateur->getLogin());
            $publications = $this->publicationService->recupererPublicationsUtilisateur($idUtilisateur);
            return $this->afficherTwig("publication/page_perso.html.twig", ["login"=>$loginHTML, "publications"=>$publications]);
        }
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    #[Route(path: '/inscription', name: 'afficherFormulaireCreation', methods: ['GET'])]
    public function afficherFormulaireCreation(): Response
    {
        return $this->afficherTwig("utilisateur/inscription.html.twig");
    }
    #[Route(path: '/inscription', name: 'creerDepuisFormulaire', methods: ['POST'])]
    public function creerDepuisFormulaire(): Response
    {
        //Recupérer les différentes variables (login, mot de passe, adresse mail, données photo de profil...)
        $login = $_POST['login'] ?? null;
        $motDePasse = $_POST['mot-de-passe'] ?? null;
        $adresseMail = $_POST['email'] ?? null;
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;
        try {
            $this->utilisateurService->creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("danger", $e->getMessage());
            return $this->rediriger("afficherFormulaireCreation");
        }
        MessageFlash::ajouter("success", "Utilisateur créé");
        return $this->rediriger("afficherListe");
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */

    #[Route(path: '/connexion', name: 'afficherFormulaireConnexion', methods: ['GET'])]
    public function afficherFormulaireConnexion(): Response
    {
        return $this->afficherTwig("utilisateur/connexion.html.twig");
    }

    /**
     * @throws ServiceException
     */
    #[Route(path: '/connexion', name: 'connecter', methods: ['POST'])]
    public function connecter(): Response
    {
        $login = $_POST['login'] ?? null;
        $mdp = $_POST['mot-de-passe'] ?? null;
        $utilisateur = $this->utilisateurService->recupererUtilisateurParLogin($login);
        try{
            $this->utilisateurService->connecter($utilisateur->getIdUtilisateur(), $mdp);
        }
        catch(ServiceException $e){
            MessageFlash::ajouter("danger", $e->getMessage());
            return $this->rediriger("afficherFormulaireConnexion");
        }
        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
        MessageFlash::ajouter("success", "Connexion effectuée.");
        return $this->rediriger("afficherListe");
    }

    #[Route(path: '/deconnexion', name: 'deconnecter', methods: ['GET'])]
    public function deconnecter(): Response
    {
        try{
            $this->utilisateurService->deconnecter();
        } catch (ServiceException $e) {
            MessageFlash::ajouter("danger", $e->getMessage());
            return $this->rediriger("afficherListe");
        }
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        return $this->rediriger("afficherListe");
    }
}
