<?php

namespace TheFeed\Controleur;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurPublication extends ControleurGenerique
{

    public function __construct(ContainerInterface $container, private PublicationServiceInterface $publicationService){
        parent::__construct($container);
    }
    /**
     * @throws Exception
     */
    #[Route(path: '/publications', name:'afficherListe', methods:['GET'])]
    #[Route(path: '/', name: '/', methods: ['$_GET'])]
    public function afficherListe() : Response
    {
        $publications = $this->publicationService->recupererPublications();
        return $this->afficherTwig('publication/feed.html.twig', [
            "publications" => $publications
        ]);
    }

    #[Route(path: '/publications', name: 'creerPubliDepuisFormulaire', methods: ['POST'])]
    public function creerPubliDepuisFormulaire() : Response
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try {
            $this->publicationService->creerPublication($idUtilisateurConnecte, $message);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("danger", $e->getMessage());
        }

        return $this->rediriger('afficherListe');
    }


}