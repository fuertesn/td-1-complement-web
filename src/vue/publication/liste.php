<?php

use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\Conteneur;
use TheFeed\Modele\DataObject\Publication;

/**
 * @var Publication[] $publications
 */
/** @var UrlGenerator $generateurUrl */
$generateurUrl = Conteneur::recupererService("generateurUrl");
/** @var UrlHelper $assistantUrl */
$assistantUrl = Conteneur::recupererService("assistantUrl");
?>
<main id="the-feed-main">
    <div id="feed">
        <?php
        if (ConnexionUtilisateur::estConnecte()) {
        ?>
            <form id="feedy-new" action="<?=$assistantUrl->getAbsoluteUrl("./publications") ?>" method="post">
                <fieldset>
                    <legend>Nouveau feedy</legend>
                    <div>
                        <label for="message"></label><textarea required id="message" minlength="1" maxlength="250" name="message" placeholder="Qu'avez-vous en tête?"></textarea>
                    </div>
                    <div>
                        <input id="feedy-new-submit" type="submit" value="Feeder!">
                    </div>
                </fieldset>
            </form>
            <?php
        }
        if (!empty($publications)) {
            foreach ($publications as $publication) {
                $loginHTML = htmlspecialchars($publication->getAuteur()->getLogin());
                $messageHTML = htmlspecialchars($publication->getMessage());
                $idAuteur = $publication->getAuteur()->getIdUtilisateur();
                $imgAuteur = $publication->getAuteur()->getNomPhotoDeProfil();
            ?>
                <div class="feedy">
                    <div class="feedy-header">
                        <a href="<?=$assistantUrl->getAbsoluteUrl("./utilisateurs/$idAuteur/publications") ?>">
                            <img class="avatar" src="<?=$assistantUrl->getAbsoluteUrl("../ressources/img/utilisateurs/$imgAuteur")?>" alt="avatar de l'utilisateur">
                        </a>
                        <div class="feedy-info">
                            <span><?= $loginHTML ?></span>
                            <span> - </span>
                            <span><?= $publication->getDate()->format('d F Y') ?></span>
                            <p><?= $messageHTML ?></p>
                        </div>
                    </div>
                </div>
            <?php
            }
        } else {
            ?>
            <p id="no-publications" class="center">Pas de publications pour le moment!</p>
        <?php
        }
        ?>
    </div>
</main>