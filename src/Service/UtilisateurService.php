<?php

namespace TheFeed\Service;

use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceInterface
{
    public function __construct(private UtilisateurRepositoryInterface $utilisateurRepository, private string $dossierPhotoDeProfil, private FileMovingServiceInterface $fileMovingService){}
    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil) : void {
        if (strlen($login) < 4 || strlen($login) > 20) {
            throw new ServiceException("Le login doit être compris entre 4 et 20 caractères!");
        }
        if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
            throw new ServiceException("Mot de passe invalide!");
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new ServiceException("L'adresse mail est incorrecte!");
        }

        $utilisateur = $this->utilisateurRepository->recupererParLogin($login);
        if ($utilisateur != null) {
            throw new ServiceException("Ce login est déjà pris!");
        }
        $utilisateur = $this->utilisateurRepository->recupererParEmail($email);
        if ($utilisateur != null) {
            throw new ServiceException("Un compte est déjà enregistré avec cette adresse mail!");
        }
        $mdpHache = MotDePasse::hacher($motDePasse);
        $explosion = explode('.', $donneesPhotoDeProfil['name']);
        $fileExtension = end($explosion);
        if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
            throw new ServiceException("La photo de profil n'est pas au bon format!");
        }
        $pictureName = uniqid() . '.' . $fileExtension;
        $from = $donneesPhotoDeProfil['tmp_name'];
        $to = $this->dossierPhotoDeProfil.$pictureName;
        $this->fileMovingService->moveFile($from, $to);
        $utilisateur = Utilisateur::create($login, $mdpHache, $email, $pictureName);
        $this->utilisateurRepository->ajouter($utilisateur);
    }

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur{
        $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur);
        if (!$autoriserNull && $utilisateur == null){
            throw new ServiceException("L'utilisateur n'existe pas");
        }
        return $utilisateur;
    }

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParLogin($login): ?Utilisateur{
        $utilisateur = $this->utilisateurRepository->recupererParLogin($login);
        if($utilisateur == null){
            throw new ServiceException("L'utilisateur n'existe pas");
        }
        return $utilisateur;
    }

    /**
     * @throws ServiceException
     */
    public function connecter($idUtilisateur, $motDePasse): void{
        if($idUtilisateur == null || $motDePasse == null){
            throw new ServiceException("Login ou mot de passe manquant");
        }
        if(!MotDePasse::verifier($motDePasse, $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur)->getMdpHache())){
            throw new ServiceException("Mot de passe incorrect");
        }
    }

    /**
     * @throws ServiceException
     */
    public function deconnecter(): void{
        if(!ConnexionUtilisateur::estConnecte()){
            throw new ServiceException("Utilisateur non connecté");
        }
        ConnexionUtilisateur::deconnecter();
    }
}