<?php

namespace TheFeed\Modele\Repository;

use PDOStatement;
use TheFeed\Modele\DataObject\Utilisateur;

interface UtilisateurRepositoryInterface
{
    public function recuperer(): array;

    public function recupererParClePrimaire($id): ?Utilisateur;

    public function recupererParLogin($login): ?Utilisateur;

    public function recupererParEmail($email): ?Utilisateur;

    public function ajouter($entite): false|string;

    public function mettreAJour($entite): void;

    public function supprimer($entite): void;

    public function extraireUtilisateur(PDOStatement $statement, array $values);
}