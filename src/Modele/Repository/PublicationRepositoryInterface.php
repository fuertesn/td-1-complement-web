<?php

namespace TheFeed\Modele\Repository;

use Exception;
use TheFeed\Modele\DataObject\Publication;

interface PublicationRepositoryInterface
{
    public function recuperer(): array;

     public function recupererParAuteur($idUtilisateur): array;

     public function ajouter(Publication $publication);

     public function recupererParClePrimaire($id): ?Publication;

     public function mettreAJour(Publication $publication);

     public function supprimer(Publication $publication);
}