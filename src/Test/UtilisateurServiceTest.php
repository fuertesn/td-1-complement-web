<?php

namespace TheFeed\Test;

use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
 use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\FileMovingServiceInterface;
 use TheFeed\Service\UtilisateurService;
class UtilisateurServiceTest extends TestCase
{
    private UtilisateurService $service;

     private UtilisateurRepositoryInterface $utilisateurRepositoryMock;

     //Dossier où seront déplacés les fichiers pendant les tests
     private string $dossierPhotoDeProfil = __DIR__."/tmp/";

     private FileMovingServiceInterface $fileMovingService;

    /**
     * @throws Exception
     */
    protected function setUp(): void
     {
         parent::setUp();
         $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
         $this->fileMovingService = $this->createMock(FileMovingServiceInterface::class);
         mkdir($this->dossierPhotoDeProfil);
         $this->service = new UtilisateurService($this->utilisateurRepositoryMock, $this->dossierPhotoDeProfil, $this->fileMovingService);
     }

    /**
     * @throws ServiceException
     */
    public function testCreerUtilisateurPhotoDeProfil() {
         $donneesPhotoDeProfil = [];
         $donneesPhotoDeProfil["name"] = "test.png";
         $donneesPhotoDeProfil["tmp_name"] = "test.png";
         $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn(null);
         $this->utilisateurRepositoryMock->method("recupererParEmail")->willReturn(null);
         $this->utilisateurRepositoryMock->method("ajouter")->willReturnCallback(function ($utilisateur) {
             self::assertTrue($utilisateur->getNomPhotoDeProfil() != null);
             return $utilisateur->getNomPhotoDeProfil();
         });
         $this->service->creerUtilisateur("test", "TestMdp123", "test@example.com", $donneesPhotoDeProfil);
     }

     protected function tearDown(): void
     {
         //Nettoyage
         parent::tearDown();
         foreach(scandir($this->dossierPhotoDeProfil) as $file) {
             if ('.' === $file || '..' === $file) continue;
             unlink($this->dossierPhotoDeProfil.$file);
         }
         rmdir($this->dossierPhotoDeProfil);
     }
}