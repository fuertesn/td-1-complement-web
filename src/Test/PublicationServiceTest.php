<?php

namespace TheFeed\Test;

use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
class PublicationServiceTest extends TestCase
{
    private PublicationService $service;
    private PublicationRepositoryInterface|MockObject $publicationRepositoryMock;
    private MockObject|UtilisateurRepositoryInterface $utilisateurRepositoryMock;

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->publicationRepositoryMock = $this->createMock(PublicationRepositoryInterface::class);
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->service = new PublicationService($this->publicationRepositoryMock, $this->utilisateurRepositoryMock);
    }

    /**
     * @throws ServiceException
     */
    public function testCreerPublicationUtilisateurInexistant(): void{
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Il faut être connecté pour publier un feed");
        $this->service->creerPublication(-1, "Test qui va passer");
    }

    public function testCreerPublicationVide(): void{
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le message ne peut pas être vide!");
        $fakeUtilisateur = new Utilisateur();
        $this->service->creerPublication($fakeUtilisateur->getIdUtilisateur(), "");
    }

    public function testCreerPublicationTropGrande(): void{
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le message ne peut pas dépasser 250 caractères!");
        $this->service->creerPublication(8, str_repeat("A", 251));
    }

    /**
     * @throws Exception
     */
    public function testNombrePublications(): void{
        $fakePublications = [new Publication(), new Publication()];
        $this->publicationRepositoryMock->method("recuperer")->willReturn($fakePublications);
        $this->assertCount(2, $this->service->recupererPublications());
    }

    /**
     * @throws Exception
     */
    public function testNombrePublicationsUtilisateur(): void {
        $publi = $this->service->recupererPublicationsUtilisateur(5);
        self::assertEquals(8, sizeof($publi));
    }

    /**
     * @throws Exception
     */
    public function testNombrePublicationsUtilisateurInexistant(): void {
        $publi = $this->service->recupererPublicationsUtilisateur(-1);
        self::assertEquals(0, sizeof($publi));
    }
}